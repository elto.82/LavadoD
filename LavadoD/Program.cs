﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LavadoD
{
    class Program
    {

        static void Main(string[] args)
        {
            string tipoVehiculo, servicio, esFiel;
            decimal valor;
            do
            {
                PedirDatos(out tipoVehiculo, out servicio, out esFiel);
                valor = CalcularValor(tipoVehiculo, servicio, esFiel);
                MostrarResultados(valor);
            } while (true);
        }

        private static void MostrarResultados(decimal valor)
        {
            Console.WriteLine("el valor a pagar es  ${0:N0} ", valor);

            Console.ReadKey();
        }

        private static decimal CalcularValor(string tipoVehiculo, string servicio, string esFiel)
        {
            decimal valor;
            valor = 0;

            if (tipoVehiculo == ("M"))
            {
                if (servicio == ("S")) valor = 7000;
                if (servicio == ("B")) valor = 12000;
                if (servicio == ("P")) valor = 15000;
            }

            if (tipoVehiculo == ("P"))
            {
                if (servicio == ("S")) valor = 11000;


                if (servicio == ("B")) valor = 15000;


                if (servicio == ("P")) valor = 20000;

            }

            if (tipoVehiculo == ("T"))
            {
                if (servicio == ("S")) valor = 8000;
                if (servicio == ("B")) valor = 13000;
                if (servicio == ("P")) valor = 17500;
            }


            if (esFiel == ("S")) valor = valor * 0.9M;
            //aplicamos iva
            valor = valor * 1.16m;

            return valor;

        }

        private static void PedirDatos(out string tipoVehiculo, out string servicio, out string esFiel)
        {
            do
            {


                Console.Write("tipo de vehiculo: [M]oto, [P]articular, [T]axi? ..............");
                tipoVehiculo = (Console.ReadLine().ToUpper());
                if (tipoVehiculo != "M" && tipoVehiculo != "P" && tipoVehiculo != "T")
                {
                    Console.WriteLine("error en su digitalizacion ");
                }
            } while (tipoVehiculo != "M" && tipoVehiculo != "P" && tipoVehiculo != "T");

            do
            {

            
            Console.Write("tipo de servicio: [S]encillo, [B]rillada, [P]etrolizada? .....");
            servicio = (Console.ReadLine().ToUpper());
                if (servicio != "S" && servicio != "B" && servicio != "P")
                {
                    Console.WriteLine("error en su digitalizacion ");

                }
            } while (servicio != "S" && servicio != "B" && servicio != "P");
            do
            {

            
            Console.Write("tiene targeta de fidelidad? [S]i, [N]o?.......................");
            esFiel = (Console.ReadLine().ToUpper());
                if (esFiel != "N" && esFiel != "S")
                {
                    Console.WriteLine("error en su digitalizacion ");

                }
            } while (esFiel != "N" && esFiel != "S");
            //  Console.ReadKey();
        }
    }
}
